package com.example.uapv1703844.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.res.Resources;


import android.widget.Toast;


public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        Intent intent = getIntent();
        final TextView textView = (TextView) findViewById(R.id.PaysNom);
        textView.setText(intent.getStringExtra("country"));
        final Country country = CountryList.getCountry(intent.getStringExtra("country"));
        final String image = intent.getStringExtra(tp1.Image);
        final Resources res = this.getResources();
        int resId = res.getIdentifier(image, "drawable", getPackageName());



        final String Image = intent.getStringExtra(tp1.Image);
        ImageView flagImage = findViewById(R.id.ImageNom);
        flagImage.setImageResource(resId);

        final String NomCapitale = intent.getStringExtra(tp1.NomCapitale);
        final EditText TextUn =(EditText) findViewById(R.id.CapitaleNom);
        TextUn.setText(NomCapitale);

        final String NomLangueOfficiel = intent.getStringExtra(tp1.NomLangueOfficiel);
        final EditText TextDeux =(EditText) findViewById(R.id.LangueOfficielNom);
        TextDeux.setText(NomLangueOfficiel);

        final String NomMonnaie = intent.getStringExtra(tp1.NomMonnaie);
        final EditText TextTrois =(EditText) findViewById(R.id.MonnaieNom);
        TextTrois.setText(NomMonnaie);

        //final String TaillePopulation = intent.getStringExtra(tp1.TaillePopulation);
        final EditText TextQuatre =(EditText) findViewById(R.id.PopulationNom);
        TextQuatre.setText(Integer.toString(country.getmPopulation()));

        //final int TailleSuperficie = intent.getIn(tp1.TailleSuperficie);
        final EditText TextCinq =(EditText) findViewById(R.id.SuperficieNom);
        TextCinq.setText(Integer.toString(country.getmArea()));

        Button saveButton = findViewById(R.id.Sauvegarder);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                country.setmCapital(TextUn.getText().toString());
                country.setmLanguage(TextDeux.getText().toString());
                country.setmCurrency(TextTrois.getText().toString());
                country.setmPopulation(Integer.parseInt(TextQuatre.getText().toString()));
                country.setmArea(Integer.parseInt(TextCinq.getText().toString()));


                finish();
            }
        });






    }
}
