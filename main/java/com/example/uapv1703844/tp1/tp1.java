package com.example.uapv1703844.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class tp1 extends AppCompatActivity {
    public static final String NomCapitale = "com.example.tp1.NomCapitale";
    public static final String NomLangueOfficiel = "com.example.tp1.NomLangueOfficiel";
    public static final String NomMonnaie = "com.example.tp1.NomMonnaie";
    public static final String TaillePopulation = "com.example.tp1.TaillePopulation";
    public static final String TailleSuperficie = "com.example.tp1.TailleSuperficie";
    public static final String Image = "com.example.tp1.Image";

    CountryList cl = new CountryList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tp1);
        final ListView listview = findViewById(R.id.MaList);
        String[] values = cl.getNameArray();

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, values);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent(tp1.this, CountryActivity.class);
                intent.putExtra("country", item);
                intent.putExtra(NomCapitale,cl.getCountry(item).getmCapital());
                intent.putExtra(NomLangueOfficiel,cl.getCountry(item).getmLanguage());
                intent.putExtra(NomMonnaie,cl.getCountry(item).getmCurrency());
                intent.putExtra(Image,cl.getCountry(item).getmImgFile());


                intent.putExtra(TaillePopulation,cl.getCountry(item).getmPopulation());



                intent.putExtra(TailleSuperficie,cl.getCountry(item).getmArea());

                startActivity(intent);
                Toast.makeText(tp1.this, "You selected: " + item, Toast.LENGTH_LONG).show();
            }
        });
    }
}
